from flask_restful import Resource
import logging as logger


class Task(Resource):
	def get(self):
		logger.debug("Inside get method")
		return {"message" : "inside get method"}, 200
